/**
 * Responsive Image Styles
 */

// Response.js is designed for jQuery 1.7+. This maps on and off to
// bind and unbind when jQuery is less than 1.7.
jQuery.fn.on = jQuery.fn.on || jQuery.fn.bind;
jQuery.fn.off = jQuery.fn.off || jQuery.fn.unbind;

(function($) {

  // $.unique enhancement to remove all duplicates
  // http://paulirish.com/2010/duck-punching-with-jquery/
  var _old = $.unique;

  $.unique = function(arr){
    // do the default behavior only if we got an array of elements
    if (!!arr[0].nodeType){
      return _old.apply(this,arguments);
    } else {
      // reduce the array to contain no dupes via grep/inArray
      return $.grep(arr,function(v,k){
        return $.inArray(v,arr) === k;
      });
    }
  };

  Drupal.behaviors.responsive_image_styles = {
    attach: function (context, settings) {

      var all_viewports = [];

      // Create an array of our breakpoints.
      $.each(Drupal.settings.responsive_image_styles.viewports, function(i, val) {
        all_viewports.push(parseInt(val, 10));
      });

      // Make sure all the break points are unique.
      all_viewports = $.unique(all_viewports);

      // Trigger Response.js with our breakpoints.
      Response.create({ prop: 'width', prefix: 'src', breakpoints: all_viewports, lazy: true });
    }
  };
})(jQuery);
