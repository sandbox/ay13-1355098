<?php
/**
 * Menu callback; Listing of all current responsive image collections.
 */
function responsive_image_styles_list() {
  $title = t('Responsive Image Collections');
  drupal_set_title($title, PASS_THROUGH);
  $page = array();

  $styles = responsive_image_styles();
  $page['responsive_image_style_list'] = array(
    '#markup' => theme('responsive_image_style_list', array('styles' => $styles)),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'image') . '/image.admin.css' => array()),
    ),
  );
  return $page;

}

/**
 * Form builder; Form for adding a new responsive image collection.
 *
 * @ingroup forms
 * @see image_style_add_form_submit()
 * @see image_style_name_validate()
 */
function responsive_image_style_add_form($form, &$form_state) {
  $title = t('Responsive Image Collections');
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#size' => '64',
    '#title' => t('Responsive collection name'),
    '#default_value' => '',
    '#description' => t('Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#element_validate' => array('responsive_image_style_name_validate'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new responsive collection'),
  );

  return $form;
}

/**
 * Form builder; Edit a responsive image collection name and breakpoints.
 *
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @param $style
 *   A responsive image collection array.
 * @ingroup forms
 * @see image_style_form_submit()
 * @see image_style_name_validate()
 */
function responsive_image_style_form($form, &$form_state, $responsive_style) {
  $title = t('Edit %name style', array('%name' => $responsive_style['name']));
  drupal_set_title($title, PASS_THROUGH);

  // Build a list of image styles to use as select options
  $new_breakpoint_styles = array();
  foreach (image_styles() as $style => $definition) {
    $style_options[$style] = check_plain($definition['name']);
  }
  $form_state['responsive_image_style'] = $responsive_style;
  $form['#tree'] = TRUE;
  $form['#attached']['css'][drupal_get_path('module', 'image') . '/image.admin.css'] = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#size' => '64',
    '#title' => t('Responsive style name'),
    '#default_value' => $responsive_style['name'],
    '#description' => t('Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#element_validate' => array('responsive_image_style_name_validate'),
    '#required' => TRUE,
  );

  // Build the list of existing image styles for this responsive group.
  $form['breakpoints'] = array(
    '#theme' => 'responsive_image_style_breakpoints',
  );
  foreach ($responsive_style['breakpoints'] as $key => $breakpoint) {
//    $form['breakpoints'][$key]['#weight'] = isset($form_state['input']['breakpoints']) ? $form_state['input']['breakpoints'][$key]['weight'] : NULL;
    $form['breakpoints'][$key]['name'] = array(
      '#markup' => $breakpoint['name'],
    );
    $form['breakpoints'][$key]['width'] = array(
      '#markup' => $breakpoint['width'].'px',
    );
    $form['breakpoints'][$key]['style'] = array(
      '#type' => 'select',
      '#title' => t('Image Style'),
      '#options'=> $style_options,
      '#default_value' => $breakpoint['style'],
      '#title_display' => 'invisible',
    );
    $form['breakpoints'][$key]['remove'] = array(
      '#type' => 'link',
      '#title' => t('delete'),
      '#href' => 'admin/config/media/image-styles/responsive/breakpoint/delete/' . $breakpoint['ribid'],
    );
  }

  // Build the new breakpoint addition form and add it to the breakpoint list.
  $form['breakpoints']['new'] = array(
    '#tree' => FALSE,
//    '#weight' => isset($form_state['input']['weight']) ? $form_state['input']['weight'] : NULL,
  );
  $form['breakpoints']['new']['new_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Breakpoint Name'),
    '#empty_option' => t('Select a new breakpoint'),
    '#title_display' => 'invisible',
  );
  $form['breakpoints']['new']['new_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum Width for image style'),
    '#title_display' => 'invisible',
    '#size' => 10,
    '#field_suffix' => 'px',
  );
  $form['breakpoints']['new']['new_style'] = array(
    '#type' => 'select',
    '#title' => t('Image Style'),
    '#options'=> $style_options,
    '#title_display' => 'invisible',
  );
  $form['breakpoints']['new']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#validate' => array('responsive_image_style_form_add_validate'),
    '#submit' => array('responsive_image_style_form_submit', 'responsive_image_style_form_add_submit'),
  );

  // Show the Override or Submit button for this style.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update responsive style'),
  );

  return $form;
}

function responsive_image_style_form_submit($form, &$form_state) {
  $style = $form_state['responsive_image_style'];

  if (isset($form_state['values']['name']) && $style['name'] != $form_state['values']['name']) {
    $style['name'] = $form_state['values']['name'];
    responsive_image_style_save($style);
  }

  // Update breakpoint styles.
  if (!empty($form_state['values']['breakpoints'])) {
    foreach ($form_state['values']['breakpoints'] as $ribid => $breakpoint_data) {
      if ($form['breakpoints'][$ribid]['style']['#default_value'] != $breakpoint_data['style']) {
        $breakpoint = $style['breakpoints'][$ribid];
        $breakpoint['style'] = $breakpoint_data['style'];
        responsive_image_breakpoint_save($breakpoint);
      }
    }
  }

  if ($form_state['values']['op'] == t('Update responsive style')) {
    drupal_set_message(t('Changes to the responsive style have been saved.'));
  }
  $form_state['redirect'] = 'admin/config/media/image-styles/responsive/edit/' . $style['name'];
}

/**
 * Element validate function to ensure unique and machine named.
 */
function responsive_image_style_name_validate($element, $form_state) {
  // Check for duplicates.
  $styles = responsive_image_styles();
  if (isset($styles[$element['#value']]) && (!isset($form_state['responsive_image_style']['risid']) || $styles[$element['#value']]['risid'] != $form_state['responsive_image_style']['risid'])) {
    form_set_error($element['#name'], t('The image style name %name is already in use.', array('%name' => $element['#value'])));
  }

  // Check for illegal characters in image style names.
  if (preg_match('/[^0-9a-z_\-]/', $element['#value'])) {
    form_set_error($element['#name'], t('Please only use lowercase alphanumeric characters, underscores (_), and hyphens (-) for style names.'));
  }
}

/**
 * Submit handler for adding a new responsive image collection.
 */
function responsive_image_style_add_form_submit($form, &$form_state) {
  $style = array('name' => $form_state['values']['name']);
  $style = responsive_image_style_save($style);
  drupal_set_message(t('Style %name was created.', array('%name' => $style['name'])));
  $form_state['redirect'] = 'admin/config/media/image-styles/responsive/edit/' . $style['name'];
}

function responsive_image_style_form_add_validate($form, &$form_state) {
  $breakpoints = responsive_image_style_breakpoints($form_state['responsive_image_style']);
  foreach($breakpoints as $breakpoint){
    if($form_state['values']['new_width'] == $breakpoint['width']){
      form_error($form['breakpoints']['new']['new_width'], t('There is already a breakpoint set for %width pixles.', array('%width' => $breakpoint['width'])));
      
    }
  }
  $styles = image_styles();
  
}

function responsive_image_style_form_add_submit($form, &$form_state) {
//  dsm($form_state);
  $breakpoint = array(
    'risid' => $form_state['responsive_image_style']['risid'],
    'name' => $form_state['values']['new_name'],
    'width' => $form_state['values']['new_width'],
    'style' => $form_state['values']['new_style'],
  );
  responsive_image_breakpoint_save($breakpoint);
}

/**
 * Form builder; Form for deleting a responsive image collection.
 */
function responsive_image_style_delete_form($form, &$form_state, $style) {
  $form_state['responsive_image_style'] = $style;

  $question = t('Are you sure you want to delete the %style style?', array('%style' => $style['name']));
  return confirm_form($form, $question, 'admin/config/media/image-styles/responsive', '', t('Delete'));
}

/**
 * Submit handler to delete an image effect.
 */
function responsive_image_style_delete_form_submit($form, &$form_state) {
  $style = $form_state['responsive_image_style'];

  responsive_image_style_delete($style);
  drupal_set_message(t('The responsive image style %name has been deleted.', array('%name' => $style['name'])));
  $form_state['redirect'] = 'admin/config/media/image-styles/responsive/';
}

/**
 * Form builder; Form for deleting a breakpoint.
 */
function responsive_image_breakpoint_delete_form($form, &$form_state, $breakpoint) {
  $form_state['responsive_image_style'] = $breakpoint['parent'];
  $form_state['responsive_image_breakpoint'] = $breakpoint;

  $question = t('Are you sure you want to delete the @breakpoint breakpoint from the %style style?', array('%style' => $breakpoint['parent']['name'], '@breakpoint' => $breakpoint['name']));
  return confirm_form($form, $question, 'admin/config/media/image-styles/responsive/edit/' . $breakpoint['parent']['name'], '', t('Delete'));
}

/**
 * Submit handler to delete a breakpoint.
 */
function responsive_image_breakpoint_delete_form_submit($form, &$form_state) {
  $style = $form_state['responsive_image_style'];
  $breakpoint = $form_state['responsive_image_breakpoint'];

  responsive_image_breakpoint_delete($breakpoint);
  drupal_set_message(t('The breakpoint %name has been deleted.', array('%name' => $breakpoint['name'])));
  $form_state['redirect'] = 'admin/config/media/image-styles/responsive/edit/' . $style['name'];
}


/**
 * Returns HTML for the page containing the list of responsive image collections.
 *
 * @param $variables
 *   An associative array containing:
 *   - styles: An array of all the responsive image collections returned by responsive_image_styles().
 *
 * @see responsive_image_styles()
 * @ingroup themeable
 */
function theme_responsive_image_style_list($variables) {
  $styles = $variables['styles'];
  $header = array(t('Style name'), t('Settings'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();
  foreach ($styles as $style) {
    $row = array();
    $row[] = l($style['name'], 'admin/config/media/image-styles/responsive/edit/' . $style['name']);
    $link_attributes = array(
      'attributes' => array(
        'class' => array('image-style-link'),
      ),
    );
//    if ($style['storage'] == IMAGE_STORAGE_NORMAL) {
      $row[] = t('Custom');
      $row[] = l(t('edit'), 'admin/config/media/image-styles/responsive/edit/' . $style['name'], $link_attributes);
      $row[] = l(t('delete'), 'admin/config/media/image-styles/responsive/delete/' . $style['name'], $link_attributes);
/*    }
    elseif ($style['storage'] == IMAGE_STORAGE_OVERRIDE) {
      $row[] = t('Overridden');
      $row[] = l(t('edit'), 'admin/config/media/image-styles/responsive/edit/' . $style['name'], $link_attributes);
      $row[] = l(t('revert'), 'admin/config/media/image-styles/responsive/revert/' . $style['name'], $link_attributes);
    }
    else {
      $row[] = t('Default');
      $row[] = l(t('edit'), 'admin/config/media/image-styles/responsive/edit/' . $style['name'], $link_attributes);
      $row[] = '';
    }*/
    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(array(
      'colspan' => 4,
      'data' => t('There are currently no responsive collections. <a href="!url">Add a new one</a>.', array('!url' => url('admin/config/media/image-styles/responsive/add'))),
    ));
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}


/**
 * Returns HTML for a listing of the image styles within a specific responsive group.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_responsive_image_style_breakpoints($variables) {
  $form = $variables['form'];

  $rows = array();

  foreach (element_children($form) as $key) {
    $row = array();
    $form[$key]['weight']['#attributes']['class'] = array('image-breakpoint-order-weight');
    if (is_numeric($key)) {
      $summary = drupal_render($form[$key]['summary']);
      $row[] = drupal_render($form[$key]['name']) . (empty($summary) ? '' : ' ' . $summary);
      $row[] = drupal_render($form[$key]['width']);
      $row[] = drupal_render($form[$key]['style']);
      $row[] = drupal_render($form[$key]['remove']);
    }
    else {
      // Add the row for adding a new image breakpoint.
//      $row[] = '<div class="image-style-new">' . drupal_render($form['new']['new']) . drupal_render($form['new']['add']) . '</div>';
      $row[] = drupal_render($form['new']['new_name']);
      $row[] = drupal_render($form['new']['new_width']);
      $row[] = drupal_render($form['new']['new_style']);
      $row[] = drupal_render($form['new']['add']);
    }

    if (!isset($form[$key]['#access']) || $form[$key]['#access']) {
      $rows[] = array(
        'data' => $row,
        'class' => !empty($form[$key]['weight']['#access']) || $key == 'new' ? array('draggable') : array(),
      );
    }
  }

  $header = array(
    t('Breakpoint Name'),
    t('Minimum Viewport Width'),
    t('Image Style'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  if (count($rows) == 1) {
    array_unshift($rows, array(array(
      'data' => t('There are currently no breakpoints in this style. Add one by selecting an option below.'),
      'colspan' => 4,
    )));
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'image-style-breakpoints')));
  return $output;
}
