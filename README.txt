
Responsive Image Styles

-- SUMMARY --

Display the correct image style for the current viewport.


-- REQUIREMENTS --

Drupal 7.x
Image Module (Core)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70501 for further information.


-- CONFIGURATION --

* Those administering Responsive Image Groups need to have the "administer
  image styles" permission. This is the same permission for Image Styles


-- USAGE --

---- Set up image styles and breakpoints ----

* Set up standard Image Styles for each breakpoint (you can reuse styles for
  multiple breakpoints)
* Go to the "Responsive Collections" tab
* Click the "Add" link and (machine) name your collection
* Add a breakpoint name and the minimum width of the viewport to use the image
  style. Then click add.
  
---- Set the formatter for your image field ----

* Add an image field to your content type if you haven already
* Go to the "Manage Display" settings of the content type
* Change the "Format" of your image field to "Responsive Image"
* In the "Format Settings" (Gear button) select the collection you want to use 


-- RESPONSIVE THEME HOOKS --

To use a responsive collection directly in your theme, use
theme_responsive_image() function in responsive_image_styles.module.


-- TODO --

The following should be completed prior to the 7.x-1.0 release:

* Add proper file styles support (current implementation is terrible)
* Make exportable for features
* Clean up any lingering image styles bits that don't apply to this module.

-- CONTRIBUTORS --

Maintainers
- Andrew Young 'ay13' <http://drupal.org/user/660686>
- Andrew Koebbe 'andrewko' <http://drupal.org/user/241418>
