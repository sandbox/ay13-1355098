<?php
/**
 *  @file
 *  Hooks available for modules to implement Styles functionality.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define information about style containers provided by a module.
 *
 * This hook enables modules to define style containers provided by this module.
 *
 * @return
 *   An array of available style containers.Each container is defined as an
 *   array keyed by the field type, each containing an associative array keyed
 *   on a machine-readable style container name, with the following items:
 *   - "label": The human-readable name of the effect.
 *   - "data": An array of data that each container might require.
 *   - "preview theme": (optional) A theme function to call when previewing
 *     a style during administration.
 *   - "help": (optional) A brief description of the style container that will
 *     be displayed to the administrator when configuring styles.
 */
/*function responsive_image_styles_styles_default_containers() {
  return array(
    'file' => array(
      'containers' => array(
        'image' => array(
          'label' => t('Responsive Image Styles'),
          'data' => array(
            'streams' => array(
              'public://',
              'private://',
            ),
            'mimetypes' => array(
              'image/png',
              'image/gif',
              'image/jpeg',
            ),
          ),
  //        'preview theme' => 'media_styles_image_style_preview',
  //        'help' => t('Image Styles will transform images to your choosing, such as by scaling and cropping. You can !manage.', array('!manage' => l(t('manage your image styles here'), 'admin/config/image/image-styles'))),
        ),
      ),
    ),
  );
}*/

function responsive_image_styles_styles_default_containers_alter(&$style) {
//  dsm($style, 'container');
  return $style;
}

function responsive_image_styles_styles_default_presets_alter(&$style) {
/*  $preset = array(
    array('name' => 'responsiveImageStyle', 'settings'=>array('responsive_image_style' => 'my_new_responsive_collection')),
    array('name' => 'thumbnail', 'settings' => array())
  );
  $style['media']['containers']['image']['presets']['my_new_responsive_collection'] = $preset;
  $style['file']['containers']['image']['presets']['my_new_responsive_collection'] = $preset;
*/  
//  dsm(image_styles(), 'image_styles');
  return $style;
}

//function responsive_image_styles_styles_default_styles() {
function responsive_image_styles_styles_default_styles_alter(&$style){
//  dsm($style, 'style');
  return $style;
} 
